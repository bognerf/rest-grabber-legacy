<?php

namespace Bognerf\RestGrabber;

use Bognerf\RestGrabber\Exceptions\HandlerException;
use Bognerf\RestGrabber\Exceptions\ValueObjectException;
use Bognerf\RestGrabber\Handlers\PlainJson;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validation;

abstract class Handler
{
    protected $raw = '';

    protected $parsed = [];

    protected $obligatoryFields = []; // Use DotNotation here

    protected $allObligatoryFieldsArePresent = true;

    protected $valueObjectsRoots = []; // Dot notation
    protected $valueObjectTypes = []; // Dot notation
    protected $valueObjects = [];
    protected $valueObjectValidationRules = [];

    public function __construct()
    {

    }

    /**
     * Get the parsed data as array
     * Calling this method before Handler::parse() makes no sense by the way
     *
     * @return array
     */
    public function get(): array
    {
        return $this->parsed;
    }

    /**
     * Parse the given $data (assuming it is JSON)
     *
     * @param string $data
     * @return Handler
     * @throws HandlerException
     * @throws Exceptions\ValueObjectException
     */
    public function parse(string $data): Handler
    {
        $this->setRaw($data);
        $this->parsed = json_decode($this->getRaw(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HandlerException('JSON cannot be parsed: ' . json_last_error_msg());
        }

        $this->checkObligatoryFields();

        $this->manufacture();

        return $this;
    }

    /**
     * Compare given obligatory fields and verify they're present
     * in the decoded (parsed) response, as well as validating its contents if applicable
     *
     * @throws HandlerException
     */
    public function checkObligatoryFields()
    {
        $validator = Validation::createValidator();

        foreach ($this->getObligatoryFields() as $dotnotation => $rule) {
            try {
                $v = DotNotation::get($this->parsed, $dotnotation);
                $violations = $validator->validate($v, $rule);
                foreach ($violations as $violation) {
                    throw new HandlerException('Validation of obligatory field failed: ' . $violation->getMessage());
                }
            } catch (ValueObjectException $e) {
                throw new HandlerException('Exception while checking obligatory fields: ' . $e->getMessage());
            }
        }
    }

    protected function manufacture(): bool
    {
        if (!$this->hasValueObjectRoots()) {
            return false;
        }

        foreach ($this->valueObjectsRoots as $root => $rules) {
            $currentRoot = DotNotation::get($this->parsed, $root);
            foreach ($currentRoot as $elem) {

                $class = $this->getValueObjectType($root);
                $vo = new $class($rules);
                $vo->setData($elem);
                $this->addValueObject($root, $vo);
            }
        }


        return true;
    }

    /**
     * Get the original data, unparsed and unchanged
     *
     * @return string
     */
    public function getRaw(): string
    {
        return $this->raw;
    }

    /**
     * Set the original data the handler has to work with
     *
     * @param string $raw
     */
    protected function setRaw(string $raw)
    {
        $this->raw = $raw;
    }

    /**
     * @return array
     */
    public function getObligatoryFields(): array
    {
        return $this->obligatoryFields;
    }

    /**
     * @param array $obligatoryFields
     */
    public function setObligatoryFields(array $obligatoryFields)
    {
        $this->obligatoryFields = $obligatoryFields;
    }

    public function allObligatoryFieldArePresent(): bool
    {
        return $this->allObligatoryFieldsArePresent;
    }

    /**
     * Determine if a root for generating value objects is specified
     *
     * @return bool
     */
    public function hasValueObjectRoots(): bool
    {
        return count($this->valueObjectsRoots) > 0;
    }

    /**
     * Gives the count of currently existing value objects
     * over all object roots
     *
     * @return int
     * @throws HandlerException
     */
    public function numValueObjects(): int
    {
        $c = 0;
        foreach ($this->valueObjectsRoots as $key => $objectsRoots) {
            $c += $this->numValueObject($key);
        }

        return $c;
    }

    /**
     * The number of actual objects within a ValueObject root
     *
     * @param string $key
     * @return int
     * @throws HandlerException
     */
    public function numValueObject(string $key): int
    {
        return count($this->valueObjects($key));
    }

    /**
     * Determine, if there are any valueObjects present
     * over all object roots
     *
     * @return bool
     * @throws HandlerException
     */
    public function hasValueObjects(): bool
    {
        return $this->numValueObjects() > 0;
    }

    /**
     * @return array
     */
    public function getValueObjectsRoots(): array
    {
        $roots = [];
        foreach ($this->valueObjectsRoots as $objectRootName => $objectsRoots) {
            $roots[] = $objectRootName;
        }
        return $roots;
    }

    /**
     * @param $valueObjectsRoot
     */
    public function addValueObjectsRoot(array $valueObjectsRoot)
    {
        $this->valueObjectsRoots = array_merge($this->valueObjectsRoots, $valueObjectsRoot);
    }

    /**
     * Check whether a ValueObject root $key is defined
     * @param string $key
     * @return bool
     */
    public function hasValueObjectRoot(string $key): bool
    {
        return isset($this->valueObjectsRoots[$key]);
    }


    /**
     * @return array
     */
    public function getValueObjectTypes(): array
    {
        return $this->valueObjectTypes;
    }

    public function getValueObjectType(string $key): string
    {
        if (!$this->hasValueObjectType($key)) {
            return ValueObject::class;
        }

        return $this->valueObjectTypes[$key];
    }

    /**
     * @param string $key
     * @param string $vo
     */
    public function addValueObjectType(string $key, string $vo)
    {
        $this->valueObjectTypes[$key] = $vo;
    }

    public function hasValueObjectType(string $key): bool
    {
        return isset($this->valueObjectTypes[$key]);
    }

    protected function addValueObject(string $key, ValueObject $vo)
    {
        $this->valueObjects[$key][] = $vo;
    }

    /**
     * @param string $key
     * @return ValueObject[]
     * @throws HandlerException
     */
    public function valueObjects(string $key): array
    {
        if (!isset($this->valueObjects[$key])) {
            throw new HandlerException('Objects root `' . $key . '` has not been defined.');
        }
        return $this->valueObjects[$key];
    }

    public function setValidationRules(array $rules)
    {
        $this->valueObjectValidationRules = $rules;
    }

    public function getValueObjectValidationRules(): array
    {
        return $this->valueObjectValidationRules;
    }
}
