<?php


namespace Bognerf\RestGrabber;

use Bognerf\RestGrabber\Exceptions\ValueObjectException;
use Symfony\Component\Validator\Validation;

/**
 * Class ValueObject
 *
 * @package Bognerf\RestGrabber
 */
class ValueObject
{
    protected $data;

    protected $validationRules = [];

    /**
     * ValueObject constructor. Pass any global $validationRules to be applied
     * to the whole response
     *
     * @param array $validationRules
     */
    public function __construct(array $validationRules = [])
    {
        $this->validationRules = $validationRules;
    }

    /**
     * Magically get properties from ValueObject's data
     *
     * @param $name
     * @return mixed
     * @throws ValueObjectException
     */
    public function __get($name)
    {
        if (!$this->has($name)) {
            throw new ValueObjectException('The property `' . $name . '` does not exist');
        }
        return $this->data[$name];
    }

    /**
     * Magically get properties from the current ValueObject by get`Fieldname`
     * where `Fieldname` will be treated with lcfirst() beforehand,
     * meaning that getMyvalue will return the element identified by `myvalue`
     *
     * @param  $name
     * @return mixed
     * @throws ValueObjectException
     */
    public function __call($method, $param)
    {
        $type = substr($method, 0, 3);
        $var = lcfirst(substr($method, 3));

        if ($type !== "get") {
            return null;
        }

        if (!$this->has($var)) {
            return null;
        }

        return $this->data[$var];
    }


    /**
     * Determine, if the property identified by $name exists
     *
     * @param  $name
     * @return bool
     */
    public function has($name): bool
    {
        return isset($this->data[$name]);
    }

    /**
     * @throws ValueObjectException
     */
    public function setData(array $data)
    {
        $this->data = $data;
        $validator = Validation::createValidator();
        foreach ($this->validationRules as $fieldName => $validationRules) {
            if (!$this->has($fieldName)) {
                throw new ValueObjectException('Fieldname `' . $fieldName . '` is unknown.');
            }

            if (!is_array($validationRules)) {
                throw new ValueObjectException('Validation rules are not valid (which is somehow funny in itself).');
            }

            $violations = $validator->validate($this->data[$fieldName], $validationRules);
            foreach ($violations as $violation) {
                throw new ValueObjectException('Validation failed for `' . $fieldName . '`: '
                    . $violation->getMessage());
            }
        }
    }

    public function toArray(): array
    {
        return $this->data;
    }
}
