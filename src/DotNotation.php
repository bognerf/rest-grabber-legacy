<?php

namespace Bognerf\RestGrabber;

use Bognerf\RestGrabber\Exceptions\ValueObjectException;

class DotNotation
{
    public const SEPARATOR = '/[:\.]/';

    /**
     * @param array $array
     * @param string $path
     * @return mixed
     * @throws ValueObjectException
     */
    public static function get(array $array, $path)
    {
        // To denote that the root element is wanted
        // use an asterisk
        if ($path === '*') {
            return $array;
        }

        if (!empty($path)) {
            $keys = static::explode($path);
            foreach ($keys as $key) {
                if (isset($array[$key])) {
                    $array = $array[$key];
                } else {
                    throw new ValueObjectException('Element `' . $path . '` not found.');
                }
            }
        }
        return $array;
    }

    /**
     * Separate dot notation
     *
     * @param  $path
     * @return array[]|false|string[]
     */
    protected static function explode($path)
    {
        return preg_split(self::SEPARATOR, $path);
    }
}
